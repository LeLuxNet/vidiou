package vidiou

type Loop struct {
	Input MiddleNode
	Loop  Expression
}

func (n Loop) streamType() StreamType {
	return n.Input.streamType()
}

func (n Loop) parents() []Node {
	return []Node{n.Input}
}

func (f Loop) cfArgs() (string, FilterOptions) {
	name := "aloop"
	if f.streamType() != Audio {
		name = name[1:]
	}
	return name, FilterOptions{
		"loop": f.Loop,
		"size": 32767,
	}
}
