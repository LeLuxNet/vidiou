package vidiou

type Scale struct {
	Input Node
	Size  Expression
}

func (n Scale) streamType() StreamType {
	return Video
}

func (n Scale) parents() []Node {
	return []Node{n.Input}
}

func (f Scale) cfArgs() (string, FilterOptions) {
	return "scale", FilterOptions{
		"s": f.Size,
	}
}
