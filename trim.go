package vidiou

type Trim struct {
	Input                 MiddleNode
	StartTime, StartFrame Expression
	EndTime, EndFrame     Expression
}

func (n Trim) parents() []Node {
	return []Node{n.Input}
}

func (n Trim) streamType() StreamType {
	return n.Input.streamType()
}

func (f Trim) cfArgs() (string, FilterOptions) {
	name := "atrim"
	if f.streamType() != Audio {
		name = name[1:]
	}
	return name, FilterOptions{
		"start":       f.StartTime,
		"start_frame": f.StartFrame,
		"end":         f.EndTime,
		"end_frame":   f.EndFrame,
	}
}
