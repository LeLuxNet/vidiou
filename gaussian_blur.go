package vidiou

type GaussianBlur struct {
	Input Node
	Sigma Expression
}

func (n GaussianBlur) parents() []Node {
	return []Node{n.Input}
}

func (n GaussianBlur) streamType() StreamType {
	return Video
}

func (f GaussianBlur) cfArgs() (string, FilterOptions) {
	return "gblur", FilterOptions{
		"sigma": f.Sigma,
	}
}
