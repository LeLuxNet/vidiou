package vidiou

type RunError string

func (e RunError) Error() string {
	return "video: run: " + string(e)
}
