package vidiou

import (
	"fmt"
	"io"
	"runtime"
	"strconv"
)

type Output interface {
	Node
	outputArgs(net network) []string
}

type WriterInput struct {
	Node     MiddleNode
	Metadata Metadata
	Options  Options
}

type Writer struct {
	io.Writer
	Inputs []WriterInput

	Metadata Metadata
	Options  Options
}

func (n *Writer) parents() []Node {
	nodes := make([]Node, len(n.Inputs))
	for i, in := range n.Inputs {
		nodes[i] = in.Node
	}
	return nodes
}

func (n *Writer) outputArgs(net network) []string {
	args := n.Options.args("")
	args = append(args, n.Metadata.args("")...)

	typeCount := make(map[StreamType]int, 5)

	for _, in := range n.Inputs {
		args = append(args, "-map", mapArg(net, n, in.Node))

		t := in.Node.streamType()
		stream := fmt.Sprintf(":%c:%d", t, typeCount[t])

		args = append(args, in.Options.args(stream)...)
		args = append(args, in.Metadata.args(":s"+stream)...)

		typeCount[t]++
	}

	return args
}

type VOutput struct {
	Input Node

	Title       string
	X, Y        int
	Size        Size
	Fullscreen  bool
	DisableQuit bool
}

func (n VOutput) parents() []Node {
	return []Node{n.Input}
}

func (o VOutput) outputArgs(net network) []string {
	a := []string{
		"-map", mapArg(net, o, o.Input),
		"-c:v", "rawvideo",
		"-pix_fmt", "rgb32",
		"-f", "sdl",
	}

	if o.Size.W != nil && o.Size.H != nil {
		a = append(a, "-window_size", o.Size.String())
	}

	if o.X != 0 {
		a = append(a, "-window_x", strconv.Itoa(o.X))
	}
	if o.Y != 0 {
		a = append(a, "-window_y", strconv.Itoa(o.Y))
	}

	if o.Fullscreen {
		a = append(a, "-window_fullscreen", "1")
	}

	if o.DisableQuit {
		a = append(a, "-window_enable_quit", "0")
	}

	return append(a, o.Title)
}

type AOutput struct {
	Input Node

	Type int
}

func (n AOutput) parents() []Node {
	return []Node{n.Input}
}

func (o AOutput) outputArgs(net network) []string {
	a := []string{
		"-map", mapArg(net, o, o.Input),
		"-f",
	}

	switch runtime.GOOS {
	default:
		a = append(a, "alsa", "default")
	}

	return a
}
