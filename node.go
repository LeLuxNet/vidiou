package vidiou

type Node interface {
	parents() []Node
}

type Filter interface {
	MiddleNode
	cfArgs() (string, FilterOptions)
}

type MiddleNode interface {
	Node
	streamType() StreamType
}
