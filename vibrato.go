package vidiou

type Vibrato struct {
	Input     Node
	Frequency Expression
	Depth     Expression
}

func (n Vibrato) streamType() StreamType {
	return Audio
}

func (n Vibrato) parents() []Node {
	return []Node{n.Input}
}

func (n Vibrato) cfArgs() (string, FilterOptions) {
	return "vibrato", FilterOptions{
		"f": n.Frequency,
		"d": n.Depth,
	}
}
