package vidiou

type Color struct {
	Color    Expression
	Size     Expression
	Rate     Expression
	Duration Expression
}

func (n Color) SetColor(l *Live, e Expression) error {
	return l.SendFrom(n, "c", e)
}

func (n Color) parents() []Node {
	return nil
}

func (n Color) streamType() StreamType {
	return Video
}

func (f Color) cfArgs() (string, FilterOptions) {
	return "color", FilterOptions{
		"c": f.Color,
		"s": f.Size,
		"r": f.Rate,
		"d": f.Duration,
	}
}
