package vidiou

var (
	Volume_Time  = "t"
	Volume_Frame = "n"
)

type Volume struct {
	Input                   Node
	VolumeOnce, VolumeFrame Expression
}

func (n Volume) parents() []Node {
	return []Node{n.Input}
}

func (n Volume) streamType() StreamType {
	return Audio
}

func (n Volume) cfArgs() (string, FilterOptions) {
	if n.VolumeFrame != nil {
		return "volume", FilterOptions{
			"volume": n.VolumeFrame,
			"eval":   "frame",
		}
	} else {
		return "volume", FilterOptions{
			"volume": n.VolumeOnce,
		}
	}
}
