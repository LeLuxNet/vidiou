package vidiou

import (
	"bytes"
	"regexp"
	"strconv"
	"time"
)

const centisecond = 10 * time.Millisecond

type Progress struct {
	Frame   int
	FPS     int
	Quality float64
	// Size in bytes
	Size int64
	Time time.Duration
	// Bitrate in kbits/s
	Bitrate float64
	Speed   float64
}

var statusRe = regexp.MustCompile(`([a-z]+)=\s*([0-9.:]+)[a-zA-Z/]*`)
var timeRe = regexp.MustCompile(`^(-?\d+):(\d+):(\d+).(\d+)$`)

func parseProgress(b []byte) Progress {
	s := Progress{}

	groups := statusRe.FindAllSubmatch(b, -1)
	for _, g := range groups {
		val := string(g[2])
		switch string(g[1]) {
		case "frame":
			n, _ := strconv.Atoi(val)
			s.Frame = n
		case "fps":
			n, _ := strconv.Atoi(val)
			s.FPS = n
		case "q":
			n, _ := strconv.ParseFloat(val, 64)
			s.Quality = n
		case "size":
			n, _ := strconv.ParseFloat(val, 64)
			s.Size = int64(n * 1024)
		case "time":
			tGroups := timeRe.FindSubmatch(g[2])

			h, _ := strconv.Atoi(string(tGroups[1]))
			min, _ := strconv.Atoi(string(tGroups[2]))
			sec, _ := strconv.Atoi(string(tGroups[3]))
			cs, _ := strconv.Atoi(string(tGroups[4]))

			s.Time = time.Duration(h)*time.Hour +
				time.Duration(min)*time.Minute +
				time.Duration(sec)*time.Second +
				time.Duration(cs)*centisecond
		case "bitrate":
			n, _ := strconv.ParseFloat(val, 64)
			s.Bitrate = n
		case "speed":
			n, _ := strconv.ParseFloat(val, 64)
			s.Speed = n
		}
	}

	return s
}

type progressHandler struct {
	progress chan<- Progress
	out      []byte
}

func (p *progressHandler) Write(b []byte) (int, error) {
	if bytes.HasPrefix(b, []byte("frame=")) || bytes.HasPrefix(b, []byte("size=")) {
		if p.progress != nil {
			p.progress <- parseProgress(b)
		}
	} else {
		p.out = append(p.out, b...)
	}

	return len(b), nil
}
