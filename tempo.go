package vidiou

type Tempo struct {
	Input Node
	Tempo Expression
}

func (n Tempo) parents() []Node {
	return []Node{n.Input}
}

func (n Tempo) streamType() StreamType {
	return Audio
}

func (n Tempo) cfArgs() (string, FilterOptions) {
	return "atempo", FilterOptions{
		"tempo": n.Tempo,
	}
}
