package vidiou

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/go-zeromq/zmq4"
)

type Binary struct {
	OverwriteFiles bool

	FFmpegPath string
}

func (b Binary) Run(progress chan<- Progress, live *Live, out ...Output) error {
	return b.RunContext(context.Background(), progress, live, out...)
}

func (b Binary) RunContext(ctx context.Context, progress chan<- Progress, live *Live, out ...Output) error {
	if len(out) == 0 {
		return errors.New("vidiou: no outputs specified")
	}

	var args []string

	if b.OverwriteFiles {
		args = append(args, "-y")
	}

	net := network{
		edges:  make(map[Node]edge),
		inputs: make(map[Input]int),
	}

	// add all nodes to the network and create edges
	for _, o := range out {
		net.add(o)
	}

	var pipeIn []io.Reader
	var pipeOut []io.Writer
	pipeIndex := 3

	// build input options
	for _, in := range net.inputsList {
		iArgs, name, err := in.inputArgs()
		if err != nil {
			return err
		}
		args = append(args, iArgs...)

		if ir, ok := in.(*Reader); ok {
			if f, ok := ir.Reader.(File); ok {
				name = string(f)
			} else {
				if len(pipeIn) == 0 {
					name = "pipe:0"
				} else {
					name = "pipe:" + strconv.Itoa(pipeIndex)
					pipeIndex++
				}
				pipeIn = append(pipeIn, ir.Reader)
			}
		}
		args = append(args, "-i", name)
	}

	// build complex filter graph
	var filter strings.Builder

	var filterI int
	var injectedZmq bool
	if live != nil {
		live.names = make(map[Node]string)
	}

	for from, edge := range net.edges {
		if f, ok := from.(Filter); ok {
			if filter.Len() != 0 {
				filter.WriteByte(';')
			}

			for _, pn := range from.parents() {
				filter.WriteByte('[')
				switch pnt := pn.(type) {
				case Stream:
					filter.WriteString(strconv.Itoa(net.inputs[pnt.Input]))
					filter.WriteByte(':')
					filter.WriteByte(byte(pnt.Type))
					filter.WriteByte(':')
					filter.WriteString(strconv.Itoa(pnt.Index))
				case Input:
					filter.WriteString(strconv.Itoa(net.inputs[pnt]))
				default:
					filter.WriteByte('x')
					filter.WriteString(strconv.Itoa(net.edges[pn][from]))
				}
				filter.WriteByte(']')
			}

			name, opt := f.cfArgs()
			if live == nil {
				cfArgs(&filter, name, opt)
			} else {
				name += "@" + strconv.Itoa(filterI)
				cfArgs(&filter, name, opt)
				live.names[from] = name
				filterI++

				if !injectedZmq {
					filter.WriteByte(',')
					if f.streamType() == Audio {
						filter.WriteByte('a')
					}
					filter.WriteString("zmq=b=")

					f, err := ioutil.TempFile("", "vidiou")
					if err != nil {
						return err
					}
					live.Addr = "ipc://" + f.Name()

					filter.WriteString(strExpression(live.Addr))
					injectedZmq = true
				}
			}

			if len(edge) > 1 {
				filter.WriteByte(',')
				if f.streamType() == Audio {
					filter.WriteByte('a')
				}
				filter.WriteString("split=")
				filter.WriteString(strconv.Itoa(len(edge)))
			}

			for _, t := range edge {
				filter.WriteByte('[')
				filter.WriteByte('x')
				filter.WriteString(strconv.Itoa(t))
				filter.WriteByte(']')
			}
		}
	}

	if filter.Len() > 0 {
		args = append(args, "-filter_complex", filter.String())
	}

	// build output mappings and options
	for _, o := range out {
		args = append(args, o.outputArgs(net)...)

		if ow, ok := o.(*Writer); ok {
			if f, ok := ow.Writer.(File); ok {
				args = append(args, string(f))
			} else {
				if len(pipeOut) == 0 {
					args = append(args, "pipe:1")
				} else {
					args = append(args, "pipe:"+strconv.Itoa(pipeIndex))
					pipeIndex++
				}
				pipeOut = append(pipeOut, ow.Writer)
			}
		}
	}

	return b.execContext(ctx, progress, pipeIn, pipeOut, live, args...)
}

func (b Binary) Exec(progress chan<- Progress, live *Live, args ...string) error {
	return b.ExecContext(context.Background(), progress, live, args...)
}

func (b Binary) ExecContext(ctx context.Context, progress chan<- Progress, live *Live, args ...string) error {
	return b.execContext(ctx, progress, nil, nil, live, args...)
}

func (b Binary) execContext(ctx context.Context, progress chan<- Progress, in []io.Reader, out []io.Writer, live *Live, args ...string) error {
	name := b.FFmpegPath
	if name == "" {
		name = "ffmpeg"
	}

	a := []string{"-nostdin", "-loglevel", "error"}
	if progress != nil {
		a = append(a, "-stats")
	}
	a = append(a, args...)
	fmt.Println(a)

	cmd := exec.CommandContext(ctx, name, a...)

	ph := &progressHandler{progress: progress}
	cmd.Stderr = ph

	var err2 error

	if len(in) > 0 {
		cmd.Stdin = in[0]
		for _, r := range in[1:] {
			if f, ok := r.(*os.File); ok {
				cmd.ExtraFiles = append(cmd.ExtraFiles, f)
			} else {
				pr, pw, err := os.Pipe()
				if err != nil {
					return err
				}
				cmd.ExtraFiles = append(cmd.ExtraFiles, pr)
				go func() {
					_, err := io.Copy(pw, r)
					if err != nil {
						err2 = err
						return
					}
					err = pw.Close()
					if err != nil {
						err2 = err
					}
				}()
			}
		}
	}

	if len(out) > 0 {
		cmd.Stdout = out[0]
		for _, w := range out[1:] {
			if f, ok := w.(*os.File); ok {
				cmd.ExtraFiles = append(cmd.ExtraFiles, f)
			} else {
				pr, pw, err := os.Pipe()
				if err != nil {
					return err
				}
				cmd.ExtraFiles = append(cmd.ExtraFiles, pw)
				go func() {
					_, err := io.Copy(w, pr)
					if err != nil {
						err2 = err
					}
				}()
			}
		}
	}

	err := cmd.Start()
	if err != nil {
		return err
	}

	if live != nil {
		c := zmq4.NewReq(ctx)

		err = c.Dial(live.Addr)
		if err != nil {
			return err
		}
		defer c.Close()

		live.s = c
	}

	err = cmd.Wait()
	if err2 != nil {
		return err2
	} else if err == nil {
		return nil
	} else if len(ph.out) == 0 {
		return err
	}

	return RunError(bytes.TrimSpace(ph.out))
}
