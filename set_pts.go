package vidiou

const (
	SetPTS_FrameRate = "FR"
	SetPTS_PTS       = "PTS"
	SetPTS_Frame     = "N"
	SetPTS_Time      = "T"
	SetPTS_StartPTS  = "STARTPTS"
	SetPTS_StartTime = "STARTT"
)

type SetPTS struct {
	Input MiddleNode
	Expression
}

func (n SetPTS) streamType() StreamType {
	return n.Input.streamType()
}

func (n SetPTS) parents() []Node {
	return []Node{n.Input}
}

func (f SetPTS) cfArgs() (string, FilterOptions) {
	name := "asetpts"
	if f.streamType() != Audio {
		name = name[1:]
	}
	return name, FilterOptions{
		"expr": f.Expression,
	}
}
