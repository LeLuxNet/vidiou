package vidiou

type StreamType byte

const (
	Video      StreamType = 'v'
	Audio      StreamType = 'a'
	Subtitle   StreamType = 's'
	Data       StreamType = 'd'
	Attachment StreamType = 't'
)

type Stream struct {
	Input *Reader
	Type  StreamType
	Index int
}

func (n Stream) streamType() StreamType {
	return n.Type
}

func (n Stream) parents() []Node {
	return []Node{n.Input}
}
