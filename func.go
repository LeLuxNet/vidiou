package vidiou

// https://ffmpeg.org/ffmpeg-utils.html#Expression-Evaluation

// Absolute value of x.
func Abs(x Expression) Expression {
	return Func("abs", x)
}

// Arccosine of x
func Acos(x Expression) Expression {
	return Func("acos", x)
}

// Arcsine of x
func Asin(x Expression) Expression {
	return Func("asin", x)
}

// Arctangent of x
func Atan(x Expression) Expression {
	return Func("atan", x)
}

func Atan2(x, y Expression) Expression {
	return Func("atan2", x, y)
}

func Between(x, min, max Expression) Expression {
	return Func("between", x, min, max)
}

func BitAnd(x, y Expression) Expression {
	return Func("bitand", x, y)
}

func BitOr(x, y Expression) Expression {
	return Func("bitor", x, y)
}

func Ceil(expr Expression) Expression {
	return Func("ceil", expr)
}

func Clip(x, min, max Expression) Expression {
	return Func("clip", x, min, max)
}

// Cosine of x
func Cos(x Expression) Expression {
	return Func("cos", x)
}

// Hyperbolic cosine of x
func Cosh(x Expression) Expression {
	return Func("cosh", x)
}

func Eq(x, y Expression) Expression {
	return Func("eq", x, y)
}

func Exp(x Expression) Expression {
	return Func("exp", x)
}

func Floor(expr Expression) Expression {
	return Func("floor", expr)
}

func Gauss(x Expression) Expression {
	return Func("gauss", x)
}

func GCD(x, y Expression) Expression {
	return Func("gcd", x, y)
}

func Gt(x, y Expression) Expression {
	return Func("gt", x, y)
}

func Gte(x, y Expression) Expression {
	return Func("gte", x, y)
}

func Hypot(x, y Expression) Expression {
	return Func("hypot", x, y)
}

func If(x, y, z Expression) Expression {
	if z == nil {
		return Func("if", x, y)
	}
	return Func("if", x, y, z)
}

func IfNot(x, y, z Expression) Expression {
	if z == nil {
		return Func("ifnot", x, y)
	}
	return Func("ifnot", x, y, z)
}

func IsInf(x Expression) Expression {
	return Func("isinf", x)
}

func IsNaN(x Expression) Expression {
	return Func("isnan", x)
}

func Load(var_ Expression) Expression {
	return Func("ld", var_)
}

func Lerp(x, y, z Expression) Expression {
	return Func("lerp", x, y, z)
}

func Log(x Expression) Expression {
	return Func("log", x)
}

func Lt(x, y Expression) Expression {
	return Func("lt", x, y)
}

func Lte(x, y Expression) Expression {
	return Func("lte", x, y)
}

func Max(x, y Expression) Expression {
	return Func("max", x, y)
}

func Min(x, y Expression) Expression {
	return Func("min", x, y)
}

func Mod(x, y Expression) Expression {
	return Func("mod", x, y)
}

func Not(expr Expression) Expression {
	return Func("not", expr)
}

func Print(t, l Expression) Expression {
	if l == nil {
		return Func("print", t)
	}
	return Func("print", t, l)
}

func Random(x Expression) Expression {
	return Func("random", x)
}

func Root(expr, max Expression) Expression {
	return Func("root", expr, max)
}

// Round returns the nearest integer, rounding half away from zero.
//  math.Round(expr)
func Round(expr Expression) Expression {
	return Func("round", expr)
}

func Sign(x Expression) Expression {
	return Func("sgn", x)
}

// Sine of x
// 	math.Sin(x)
func Sin(x Expression) Expression {
	return Func("sin", x)
}

// Hyperbolic sine of x
// 	math.Sinh(x)
func Sinh(x Expression) Expression {
	return Func("sinh", x)
}

func Sqrt(expr Expression) Expression {
	return Func("sqrt", expr)
}

func Squish(x Expression) Expression {
	return Func("squish", x)
}

func Store(var_, expr Expression) Expression {
	return Func("st", var_, expr)
}

// Tangent of x
// 	math.Tan(x)
func Tan(x Expression) Expression {
	return Func("tan", x)
}

// Hyperbolic tangent of x
// 	math.Tanh(x)
func Tanh(x Expression) Expression {
	return Func("tanh", x)
}

func Taylor(expr, x, id Expression) Expression {
	if id == nil {
		return Func("taylor", expr, x)
	}
	return Func("taylor", expr, x, id)
}

// Current time in seconds
// 	time.Now().Unix()
var Time = Func("time", 0)

// Round expr towards 0
// 	math.Trunc(expr)
func Trunc(expr Expression) Expression {
	return Func("trunc", expr)
}

func While(cond, expr Expression) Expression {
	return Func("while", cond, expr)
}
