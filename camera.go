package vidiou

import (
	"fmt"
	"runtime"
	"strconv"
)

type Camera struct {
	Name  string
	Index int

	Format      string
	PixelFormat string
}

func (n Camera) streamType() StreamType {
	return Video
}

func (n Camera) parents() []Node {
	return nil
}

func (i Camera) inputArgs() ([]string, string, error) {
	a := []string{"-f"}

	name := i.Name

	switch runtime.GOOS {
	case "windows":
		a = append(a, "dshow")

		if i.PixelFormat != "" {
			a = append(a, "-pixel_format", i.PixelFormat)
		}

		if i.Index != 0 {
			a = append(a, "-video_device_number", strconv.Itoa(i.Index))
		}

		name = fmt.Sprintf(`video="%s"`, i.Name)
	case "darwin":
		a = append(a, "avfoundation")

		if i.Index != 0 {
			a = append(a, "-video_device_index", strconv.Itoa(i.Index))
		}
	default:
		a = append(a, "v4l2")

		if i.Format != "" {
			a = append(a, "-input_format", i.Format)
		}

		if name == "" {
			name = fmt.Sprintf("/dev/video%d", i.Index)
		}
	}

	return a, name, nil
}
