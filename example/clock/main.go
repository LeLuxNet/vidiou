package main

import (
	"fmt"
	"os"
	"time"

	v "lelux.net/vidiou"
)

func main() {
	size := v.SizeHD1080

	// calculate time components from unix seconds
	_, offset := time.Now().Zone()
	unixSec := v.Add(v.Time, offset)

	hour := v.Mod(v.Div(unixSec, 60*60), 24)
	min := v.Mod(v.Div(unixSec, 60), 60)
	sec := v.Mod(unixSec, 60)

	// format timeStr to string
	timeStr := v.Format("%d:%2d:%2d", hour, min, sec)

	// orange unicolor background
	background := v.Color{
		Color: v.ColorCoral,
		Size:  size,
	}

	// centered black text on background
	frame := v.Text{
		Input:    background,
		X:        v.Text_XCenter,
		Y:        v.Text_YCenter,
		FontSize: v.Div(size.H, 5),
		Text:     timeStr,
	}

	window := v.VOutput{
		Input: frame,
		Title: "Clock",
		// Fullscreen: true,
	}

	bin := v.Binary{}
	err := bin.Run(nil, nil, window)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
