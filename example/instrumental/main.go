package main

import (
	"fmt"
	"io"
	"os"

	v "lelux.net/vidiou"
)

func main() {
	var audio io.Reader
	if len(os.Args) < 2 {
		audio = os.Stdin
	} else {
		audio = v.File(os.Args[1])
	}

	// extracts the instumental by getting the songs side channel
	// this is done by subtracting the left channel from the right channel
	// the vocals mostly live in the center and are therefore filtered out
	// this is not a perfect method but works good enough for this example
	side := v.Sub(v.AEval_Value(0), v.AEval_Value(1))

	instrumental := &v.AEval{
		Input:       &v.Reader{Reader: audio},
		Expressions: []v.Expression{side},
	}

	player := v.AOutput{
		Input: v.Realtime{
			Input: instrumental,
		},
	}

	bin := v.Binary{}
	err := bin.Run(nil, nil, player)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
