package main

import (
	"fmt"
	"os"

	v "lelux.net/vidiou"
)

func Ptr[T any](v T) *T {
	return &v
}

func main() {
	audio := v.Stream{
		Input: &v.Reader{Reader: v.File("https://www.nps.gov/edis/learn/photosmultimedia/upload/EDIS-SRP-0197-09.mp3")},
		Type:  v.Audio,
	}

	cover := v.Stream{
		Input: &v.Reader{Reader: v.File("https://upload.wikimedia.org/wikipedia/commons/1/11/Beethoven_Piano_Sonata_14_-_title_page_1802.jpg")},
		Type:  v.Video,
	}

	out := &v.Writer{
		Writer: v.File("out.mp3"),
		Inputs: []v.WriterInput{
			{Node: audio},
			{Node: cover},
		},
		Metadata: v.Metadata{
			Title:     Ptr(""),
			Album:     Ptr("Piano Sonata No. 14"),
			Artist:    Ptr("Ludwig van Beethoven"),
			Performer: Ptr("Bellini Ensemble Unique"),
			Year:      Ptr(1924),
		},
	}

	bin := v.Binary{OverwriteFiles: true}
	err := bin.Run(nil, nil, out)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
