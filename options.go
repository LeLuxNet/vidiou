package vidiou

import (
	"fmt"
	"strconv"
)

type Options struct {
	Format      string
	Codec       string
	Bitrate     int
	Preset      Preset
	PixelFormat string

	Custom map[string]string
}

func (o Options) args(stream string) []string {
	var args []string

	if o.Format != "" {
		args = append(args, "-f"+stream, o.Format)
	}
	if o.Codec != "" {
		args = append(args, "-c"+stream, o.Codec)
	}
	if o.Bitrate != 0 {
		args = append(args, "-b"+stream, strconv.Itoa(o.Bitrate))
	}
	if o.Preset != "" {
		args = append(args, "-preset", string(o.Preset))
	}
	if o.PixelFormat != "" {
		args = append(args, "-pix_fmt", o.PixelFormat)
	}

	for key, val := range o.Custom {
		args = append(args, "-"+key, val)
	}

	return args
}

func mapArg(net network, out Node, n Node) string {
	switch nt := n.(type) {
	case Stream:
		return fmt.Sprintf("%d:%c:%d", net.inputs[nt.Input], nt.Type, nt.Index)
	case Input:
		return strconv.Itoa(net.inputs[nt])
	default:
		return fmt.Sprintf("[x%d]", net.edges[nt][out])
	}
}

type Metadata struct {
	Title       *string
	Description *string
	Comment     *string
	Artist      *string
	Album       *string
	Genre       *string
	Year        *int
	Composer    *string
	Performer   *string
	Language    *string
	Copyright   *string

	Custom map[string]string
}

func (m Metadata) args(stream string) []string {
	var args []string

	meta := "-metadata" + stream

	args = addMeta(args, meta, "title", m.Title)
	args = addMeta(args, meta, "description", m.Description)
	args = addMeta(args, meta, "comment", m.Comment)
	args = addMeta(args, meta, "artist", m.Artist)
	args = addMeta(args, meta, "album", m.Album)
	args = addMeta(args, meta, "genre", m.Genre)
	if m.Year != nil {
		args = append(args, meta, "date="+strconv.Itoa(*m.Year))
	}
	args = addMeta(args, meta, "composer", m.Composer)
	args = addMeta(args, meta, "performer", m.Performer)
	args = addMeta(args, meta, "language", m.Language)
	args = addMeta(args, meta, "copyright", m.Copyright)

	for key, val := range m.Custom {
		args = append(args, meta, key+"="+val)
	}

	return args
}

func addMeta(args []string, meta string, key string, val *string) []string {
	if val != nil {
		args = append(args, meta, key+"="+*val)
	}
	return args
}
