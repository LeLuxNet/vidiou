package vidiou

import (
	"fmt"
	"image/color"
	"strconv"
	"strings"
	"time"
)

type Expression interface{}

var escaper = strings.NewReplacer(
	`\`, `\\\\\\\\`,
	`'`, `\\\'`,
	`:`, `\\:`,
	`[`, `\\\[`,
	`]`, `\\\]`,
	`,`, `\\\,`,
	`;`, `\\\;`,
)

type rawString string

func strExpression(e Expression) string {
	switch e := e.(type) {
	case rawString:
		return string(e)
	case bool:
		if e {
			return "1"
		} else {
			return "0"
		}
	case time.Duration:
		return strconv.FormatFloat(e.Seconds(), 'f', -1, 64)
	case color.Color:
		c := color.NRGBAModel.Convert(e).(color.NRGBA)
		return fmt.Sprintf("#%02x%02x%02x%02x", c.R, c.G, c.B, c.A)
	case string:
		return escaper.Replace(e)
	default:
		return fmt.Sprint(e)
	}
}

func arithmetic(exprs []Expression, sep string) Expression {
	str := make([]string, len(exprs))
	for i, e := range exprs {
		str[i] = "(" + strExpression(e) + ")"
	}
	return rawString(strings.Join(str, sep))
}

func Add(e ...Expression) Expression {
	return arithmetic(e, "+")
}

func Sub(e ...Expression) Expression {
	return arithmetic(e, "-")
}

func Mul(e ...Expression) Expression {
	return arithmetic(e, "*")
}

func Div(e ...Expression) Expression {
	return arithmetic(e, "/")
}

func Pow(e ...Expression) Expression {
	return arithmetic(e, "^")
}

func Func(name string, params ...Expression) Expression {
	str := make([]string, len(params))
	for i, p := range params {
		str[i] = strExpression(p)
	}
	return rawString(fmt.Sprintf("%s(%s)", name, strings.Join(str, "\\,")))
}

func Format(format string, e ...Expression) Expression {
	var b strings.Builder
	b.Grow(len(format))

	eIndex := 0
	strBegin := 0

	for i := 0; i < len(format); {
		if format[i] == '%' {
			b.WriteString(strExpression(format[strBegin:i]))
			i++

			paddingBegin := i
			for '0' <= format[i] && format[i] <= '9' {
				i++
			}

			switch format[i] {
			case 'f':
				b.WriteString(`%{expr\\:`)
				b.WriteString(strExpression(e[eIndex]))
				eIndex++
				b.WriteByte('}')
			case 'x', 'X', 'd', 'u':
				b.WriteString(`%{eif\\:`)
				b.WriteString(strExpression(e[eIndex]))
				eIndex++
				b.WriteString(`\\:`)
				b.WriteByte(format[i])
				if paddingBegin != i {
					b.WriteString(`\\:`)
					b.WriteString(format[paddingBegin:i])
				}
				b.WriteByte('}')
			}
			i++
			strBegin = i
		} else {
			i++
		}
	}

	b.WriteString(strExpression(format[strBegin:]))

	return rawString(b.String())
}
