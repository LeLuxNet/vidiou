package vidiou

const (
	Hue_Time  = "t"
	Hue_Frame = "n"
)

type Hue struct {
	Input      Node
	Hue        Expression
	Saturation Expression
	Brightness Expression
}

func (n Hue) parents() []Node {
	return []Node{n.Input}
}

func (n Hue) streamType() StreamType {
	return Video
}

func (f Hue) cfArgs() (string, FilterOptions) {
	return "hue", FilterOptions{
		"s": f.Saturation,
		"H": f.Hue,
		"b": f.Brightness,
	}
}
