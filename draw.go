package vidiou

type Grid struct {
	Input     Node
	X, Y      Expression
	Size      Size
	Color     Expression
	Thickness Expression
}

func (n Grid) parents() []Node {
	return []Node{n.Input}
}

func (n Grid) streamType() StreamType {
	return Video
}

func (f Grid) cfArgs() (string, FilterOptions) {
	return "drawgrid", FilterOptions{
		"x": f.X,
		"y": f.Y,
		"w": f.Size.W,
		"h": f.Size.H,
		"c": f.Color,
		"t": f.Thickness,
	}
}

const (
	Text_Width      = "w"
	Text_Height     = "h"
	Text_TextWidth  = "tw"
	Text_TextHeight = "th"
	Text_X          = "x"
	Text_Y          = "y"
	Text_Time       = "t"
	Text_Frame      = "n"
)

var (
	Text_XCenter = Div(Sub(Text_Width, Text_TextWidth), 2)
	Text_YCenter = Div(Sub(Text_Height, Text_TextHeight), 2)
)

type Text struct {
	Input Node
	X, Y  Expression
	Text  Expression

	Font      Expression
	FontSize  Expression
	FontColor Expression
}

func (n Text) parents() []Node {
	return []Node{n.Input}
}

func (n Text) streamType() StreamType {
	return Video
}

func (f Text) cfArgs() (string, FilterOptions) {
	return "drawtext", FilterOptions{
		"x":              f.X,
		"y":              f.Y,
		"text":           f.Text,
		"font":           f.Font,
		"fontsize":       f.FontSize,
		"fontcolor_expr": f.FontColor,
	}
}
