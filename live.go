package vidiou

import (
	"bytes"
	"errors"
	"fmt"
	"strconv"
	"sync"

	"github.com/go-zeromq/zmq4"
)

var (
	errParseLive = errors.New("vidiou: unable to parse live response")
)

type Live struct {
	s     zmq4.Socket
	sLock sync.Mutex

	Addr string

	names map[Node]string
}

func (l *Live) SendFrom(target Node, cmd string, args ...Expression) error {
	return l.Send(l.names[target], cmd, args...)
}

func (l *Live) Send(target, cmd string, args ...Expression) error {
	var b bytes.Buffer
	b.WriteString(target)
	b.WriteByte(' ')
	b.WriteString(cmd)
	for _, a := range args {
		b.WriteByte(' ')
		b.WriteString(strExpression(a))
	}

	l.sLock.Lock()
	defer l.sLock.Unlock()

	err := l.s.Send(zmq4.NewMsg(b.Bytes()))
	if err != nil {
		return err
	}

	m, err := l.s.Recv()
	if err != nil {
		return err
	}

	res := m.Frames[0]

	codeI := bytes.IndexByte(res, ' ')
	if codeI == -1 {
		return errParseLive
	}

	code, err := strconv.Atoi(string(res[:codeI]))
	if err != nil {
		return err
	}

	fmt.Println(code)

	return nil
}
