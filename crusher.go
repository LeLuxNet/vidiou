package vidiou

type Crusher struct {
	Input        Node
	LevelIn      Expression
	LevelOut     Expression
	Bits         Expression
	Logarithmic  Expression
	AntiAliasing Expression
}

func (n Crusher) parents() []Node {
	return []Node{n.Input}
}

func (n Crusher) streamType() StreamType {
	return Audio
}

func (f Crusher) cfArgs() (string, FilterOptions) {
	return "acrusher", FilterOptions{
		"level_in":  f.LevelIn,
		"level_out": f.LevelOut,
		"bits":      f.Bits,
		"mode":      If(f.Logarithmic, "log", "lin"),
		"aa":        f.AntiAliasing,
	}
}
