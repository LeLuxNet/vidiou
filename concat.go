package vidiou

type Concat [][]MiddleNode

func (n Concat) parents() []Node {
	var in []Node
	for _, segment := range n {
		for _, n := range segment {
			in = append(in, n)
		}
	}
	return in
}

func (n Concat) streamType() StreamType {
	return n[0][0].streamType()
}

func (f Concat) cfArgs() (string, FilterOptions) {
	var v int
	var a int
	for _, n := range f[0] {
		if n.streamType() == Video {
			v++
		} else {
			a++
		}
	}

	return "concat", FilterOptions{
		"n": len(f),
		"v": v,
		"a": a,
	}
}
