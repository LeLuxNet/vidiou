package vidiou

// import (
// 	"bytes"
// 	"fmt"
// 	"image"
// 	"image/color"
// 	"strconv"
// )

// func Image(m image.Image) Input {
// 	var format string
// 	var data []byte

// 	b := m.Bounds()

// 	switch m := m.(type) {
// 	case *image.Gray:
// 		format = "gray8"
// 		data = imageBytes(m, m.Pix, 1)
// 	case *image.Gray16:
// 		format = "gray16be"
// 		data = imageBytes(m, m.Pix, 2)
// 	case *image.NRGBA:
// 		format = "rgba"
// 		data = imageBytes(m, m.Pix, 4)
// 	case *image.NRGBA64:
// 		format = "rgba64"
// 		data = imageBytes(m, m.Pix, 8)

// 	case *image.YCbCr:
// 		switch m.SubsampleRatio {
// 		case image.YCbCrSubsampleRatio444:
// 			format = "yuv444p"
// 		case image.YCbCrSubsampleRatio422:
// 			format = "yuv422p"
// 		// case image.YCbCrSubsampleRatio420:
// 		// 	format = "yuv420p"
// 		// case image.YCbCrSubsampleRatio440:
// 		// 	format = "yuv440p"
// 		// case image.YCbCrSubsampleRatio411:
// 		// 	format = "yuv411p"
// 		// case image.YCbCrSubsampleRatio410:
// 		// 	format = "yuv410p"
// 		default:
// 			return nil
// 		}

// 		fmt.Println(len(m.Y), len(m.Cb), len(m.Cr))
// 		fmt.Println(m.YStride, m.CStride, m.SubsampleRatio)

// 		yBlock := m.YStride * b.Dy()
// 		cBlock := m.CStride * b.Dy()

// 		data = make([]byte, yBlock+cBlock+cBlock)
// 		copy(data, m.Y)
// 		copy(data[yBlock:], m.Cb)
// 		copy(data[yBlock+cBlock:], m.Cr)

// 	default:
// 		format = "rgba"
// 		data = make([]byte, b.Dx()*b.Dy()*4)

// 		i := 0
// 		for y := b.Min.Y; y < b.Max.Y; y++ {
// 			for x := b.Min.X; x < b.Max.X; x++ {
// 				c := color.NRGBA64Model.Convert(m.At(x, y)).(color.NRGBA)
// 				data[i] = c.R
// 				data[i+1] = c.G
// 				data[i+2] = c.B
// 				data[i+3] = c.A
// 				i += 4
// 			}
// 		}
// 	}

// 	size := strconv.Itoa(b.Dx()) + "x" + strconv.Itoa(b.Dy())
// 	return &Reader{
// 		Reader: bytes.NewReader(data),
// 		Options: FileOptions{
// 			Format: "rawvideo",
// 			Custom: [][2]string{
// 				{"pixel_format", format},
// 				{"video_size", size},
// 			}}}
// }

// type img interface {
// 	Bounds() image.Rectangle
// 	PixOffset(x, y int) int
// }

// func imageBytes(m img, pix []uint8, pixSize int) []byte {
// 	b := m.Bounds()
// 	data := make([]byte, b.Dx()*b.Dy()*pixSize)

// 	i := 0
// 	for y := b.Min.Y; y < b.Max.Y; y++ {
// 		for x := b.Min.X; x < b.Max.X; x++ {
// 			po := m.PixOffset(x, y)
// 			copy(data[i:], pix[po:po+pixSize])
// 			i += pixSize
// 		}
// 	}

// 	return data
// }
