package vidiou

import (
	"strconv"
	"strings"
)

type LV2 struct {
	Input    Node
	Plugin   string
	Controls map[string]float64
}

func (n *LV2) parents() []Node {
	return []Node{n.Input}
}

func (n *LV2) streamType() StreamType {
	return Audio
}

func (f *LV2) cfArgs() (string, FilterOptions) {
	var c strings.Builder

	first := true
	for key, val := range f.Controls {
		if !first {
			c.WriteByte('|')
			first = true
		}
		c.WriteString(key)
		c.WriteByte('=')
		c.WriteString(strconv.FormatFloat(val, 'f', -1, 64))
	}

	return "lv2", FilterOptions{
		"p": f.Plugin,
		"c": c.String(),
	}
}
