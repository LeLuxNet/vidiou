package vidiou

type Mix struct {
	Inputs []MiddleNode
}

func (n Mix) parents() []Node {
	p := make([]Node, len(n.Inputs))
	for i, child := range n.Inputs {
		p[i] = child
	}
	return p
}

func (n Mix) streamType() StreamType {
	t := n.Inputs[0].streamType()
	for _, it := range n.Inputs[1:] {
		if t != it.streamType() {
			panic("vidiou.Mix: only inputs of the same type are supported")
		}
	}
	return t
}

func (f Mix) cfArgs() (string, FilterOptions) {
	name := "amix"
	if f.streamType() != Audio {
		name = name[1:]
	}
	return name, FilterOptions{
		"inputs": len(f.Inputs),
	}
}
