package vidiou

type VWaveform struct {
	Input  Node
	Size   Expression
	Rate   Expression
	Colors []Expression
}

func (n *VWaveform) parents() []Node {
	return []Node{n.Input}
}

func (n *VWaveform) streamType() StreamType {
	return Video
}

func (f *VWaveform) cfArgs() (string, FilterOptions) {
	return "showwaves", FilterOptions{
		"s":      f.Size,
		"r":      f.Rate,
		"colors": arithmetic(f.Colors, "|"),
	}
}
