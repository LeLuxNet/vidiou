package vidiou

const (
	Overlay_MainWidth     = "W"
	Overlay_MainHeight    = "H"
	Overlay_OverlayWidth  = "w"
	Overlay_OverlayHeight = "h"
	Overlay_X             = "x"
	Overlay_Y             = "y"
	Overlay_Time          = "t"
	Overlay_Frame         = "n"
)

type Overlay struct {
	Main, Overlay Node
	X, Y          Expression
}

func (n Overlay) parents() []Node {
	return []Node{n.Main, n.Overlay}
}

func (n Overlay) streamType() StreamType {
	return Video
}

func (f Overlay) cfArgs() (string, FilterOptions) {
	return "overlay", FilterOptions{
		"x": f.X,
		"y": f.Y,
	}
}
