package vidiou

type Resample struct {
	Input      Node
	SampleRate Expression
}

func (n Resample) parents() []Node {
	return []Node{n.Input}
}

func (n Resample) streamType() StreamType {
	return Audio
}

func (n Resample) cfArgs() (string, FilterOptions) {
	return "aresample", FilterOptions{
		"osr": n.SampleRate,
	}
}
