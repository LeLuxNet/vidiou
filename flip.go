package vidiou

type Flip struct {
	Input      Node
	Horizontal bool
}

func (n Flip) parents() []Node {
	return []Node{n.Input}
}

func (n Flip) streamType() StreamType {
	return Video
}

func (f Flip) cfArgs() (string, FilterOptions) {
	if f.Horizontal {
		return "hflip", nil
	}
	return "vflip", nil
}
