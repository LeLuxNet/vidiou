package vidiou

import (
	"strings"
)

type FilterOptions map[string]Expression

func cfArgs(b *strings.Builder, name string, opt FilterOptions) {
	b.WriteString(name)

	if len(opt) == 0 {
		return
	}

	first := true
	for key, val := range opt {
		if val == nil {
			continue
		}

		sVal := strExpression(val)
		if sVal == "" {
			continue
		}

		if first {
			b.WriteByte('=')
		} else {
			b.WriteByte(':')
		}
		first = false

		b.WriteString(key)
		b.WriteByte('=')
		b.WriteString(sVal)
	}
}
