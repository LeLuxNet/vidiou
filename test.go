package vidiou

type Test struct {
	Size     Expression
	Rate     Expression
	Duration Expression
}

func (n Test) parents() []Node {
	return nil
}

func (n Test) streamType() StreamType {
	return Video
}

func (f Test) cfArgs() (string, FilterOptions) {
	return "testsrc", FilterOptions{
		"s": f.Size,
		"r": f.Rate,
		"d": f.Duration,
	}
}
