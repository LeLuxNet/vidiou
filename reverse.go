package vidiou

type Reverse struct {
	Input Node
}

func (n Reverse) parents() []Node {
	return []Node{n.Input}
}

func (n Reverse) streamType() StreamType {
	return Audio
}

func (n Reverse) cfArgs() (string, FilterOptions) {
	return "areverse", nil
}
