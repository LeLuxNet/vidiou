package vidiou

type Sine struct {
	Frequency  Expression
	SampleRate Expression
	Duration   Expression
}

func (n Sine) streamType() StreamType {
	return Audio
}

func (n Sine) parents() []Node {
	return nil
}

func (f Sine) cfArgs() (string, FilterOptions) {
	return "sine", FilterOptions{
		"f": f.Frequency,
		"r": f.SampleRate,
		"d": f.Duration,
	}
}
