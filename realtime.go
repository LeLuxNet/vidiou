package vidiou

type Realtime struct {
	Input MiddleNode
	Speed Expression
}

func (n Realtime) parents() []Node {
	return []Node{n.Input}
}

func (n Realtime) streamType() StreamType {
	return n.Input.streamType()
}

func (f Realtime) cfArgs() (string, FilterOptions) {
	name := "arealtime"
	if f.streamType() != Audio {
		name = name[1:]
	}
	return name, FilterOptions{
		"speed": f.Speed,
	}
}
