package vidiou

import (
	"io"
)

type Input interface {
	Node
	inputArgs() ([]string, string, error)
}

type Reader struct {
	io.Reader

	Options Options
}

func (n *Reader) parents() []Node {
	return nil
}

func (n *Reader) inputArgs() ([]string, string, error) {
	return n.Options.args(""), "", nil
}

type Null struct {
	Type StreamType
	Size Expression
}

func (n Null) parents() []Node {
	return nil
}

func (n Null) streamType() StreamType {
	return n.Type
}

func (f Null) cfArgs() (string, FilterOptions) {
	name := "anullsrc"
	if f.streamType() != Audio {
		name = name[1:]
	}
	return name, FilterOptions{
		"s": f.Size,
	}
}
