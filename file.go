package vidiou

type File string

func (f File) Read([]byte) (int, error) {
	panic("vidiou.File is not a real io.Reader. It can only be used as an vidiou input.")
}

func (f File) Write([]byte) (int, error) {
	panic("vidiou.File is not a real io.Writer. It can only be used as an vidiou output.")
}
