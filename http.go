package vidiou

import (
	"encoding/hex"
	"io"
	"net/http"
	"os"
	"strings"
)

type HTTP struct {
	*http.Request
}

func (n HTTP) parents() []Node {
	return nil
}

func (n HTTP) inputArgs() ([]string, string, error) {
	var args []string

	args = append(args, "-method", n.Method)

	var headers strings.Builder
	n.Header.Write(&headers)
	args = append(args, "-headers", headers.String())

	if n.Body != nil {
		var b strings.Builder
		w := hex.NewEncoder(&b)
		_, err := io.Copy(w, n.Body)
		if err != nil {
			return nil, "", err
		}
		err = n.Body.Close()
		if err != nil {
			return nil, "", err
		}
		args = append(args, "-post_data", b.String())
	}

	os.Pipe()

	return args, n.URL.String(), nil
}
