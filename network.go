package vidiou

type edge map[Node]int

type network struct {
	edges     map[Node]edge
	nodeCount int

	inputs     map[Input]int
	inputsList []Input
}

func (net *network) add(n Node) {
	for _, pn := range n.parents() {
		if net.addEdge(pn, n) {
			net.add(pn)
		}
	}
}

func (net *network) addEdge(left, right Node) bool {
	if e, ok := net.edges[left]; ok {
		if _, ok := e[right]; ok {
			// edge is already known
			return false
		} else {
			// only left point is already known
			e[right] = net.nodeCount
		}
	} else {
		net.edges[left] = edge{right: net.nodeCount}

		// inputs can only occur on the left side
		if in, ok := left.(Input); ok {
			net.inputs[in] = len(net.inputsList)
			net.inputsList = append(net.inputsList, in)
		}
	}

	net.nodeCount++

	return true
}
