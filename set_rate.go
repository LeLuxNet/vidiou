package vidiou

type SetRate struct {
	Input      Node
	SampleRate Expression
}

func (n SetRate) parents() []Node {
	return []Node{n.Input}
}

func (n SetRate) streamType() StreamType {
	return Audio
}

func (n SetRate) cfArgs() (string, FilterOptions) {
	return "asetrate", FilterOptions{
		"r": n.SampleRate,
	}
}
