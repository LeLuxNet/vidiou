module lelux.net/vidiou

go 1.18

require github.com/go-zeromq/zmq4 v0.14.1

require (
	github.com/go-zeromq/goczmq/v4 v4.2.2 // indirect
	golang.org/x/sync v0.0.0-20220513210516-0976fa681c29 // indirect
)
