package vidiou

const (
	Rotate_Time  = "t"
	Rotate_Frame = "n"
)

type Rotate struct {
	Input      Node
	AngleFrame Expression
}

func (n Rotate) parents() []Node {
	return []Node{n.Input}
}

func (n Rotate) streamType() StreamType {
	return Video
}

func (n Rotate) cfArgs() (string, FilterOptions) {
	return "rotate", FilterOptions{
		"a": n.AngleFrame,
	}
}
