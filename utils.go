package vidiou

import (
	"image/color"
	"strings"
)

type Size struct {
	W, H Expression
}

func (s Size) String() string {
	return strExpression(s.W) + "x" + strExpression(s.H)
}

var (
	SizeNTSC     = Size{720, 480}
	SizePAL      = Size{720, 576}
	SizeQNTSC    = Size{352, 240}
	SizeQPAL     = Size{352, 288}
	SizeSNTSC    = Size{640, 480}
	SizeSPAL     = Size{768, 576}
	SizeFilm     = SizeQNTSC
	SizeNTSCFilm = SizeQNTSC
	SizeSQCIF    = Size{128, 96}
	SizeQCIF     = Size{176, 144}
	SizeCIF      = Size{352, 288}
	Size4CIF     = Size{704, 576}
	Size16CIF    = Size{1408, 1152}
	SizeQQVGA    = Size{160, 120}
	SizeQVGA     = Size{320, 240}
	SizeVGA      = Size{640, 480}
	SizeSVGA     = Size{800, 600}
	SizeXGA      = Size{1024, 768}
	SizeUXGA     = Size{1600, 1200}
	SizeQXGA     = Size{2048, 1536}
	SizeSXGA     = Size{1280, 1024}
	SizeQSXGA    = Size{2560, 2048}
	SizeHSXGA    = Size{5120, 4096}
	SizeWVGA     = Size{852, 480}
	SizeWXGA     = Size{1366, 768}
	SizeWSXGA    = Size{1600, 1024}
	SizeWUXGA    = Size{1920, 1200}
	SizeWOXGA    = Size{2560, 1600}
	SizeWQHD     = Size{2560, 1440}
	SizeWQSXGA   = Size{3200, 2048}
	SizeWQUXGA   = Size{3840, 2400}
	SizeWHSXGA   = Size{6400, 4096}
	SizeWHUXGA   = Size{7680, 4800}
	SizeCGA      = Size{320, 200}
	SizeEGA      = Size{640, 350}
	SizeHD480    = Size{852, 480}
	SizeHD720    = Size{1280, 720}
	SizeHD1080   = Size{1920, 1080}
	// SizeQHD      = Size{2560, 1440}
	Size2K      = Size{2048, 1080}
	Size2KDCI   = Size2K
	Size2KFlat  = Size{1998, 1080}
	Size2KScope = Size{2048, 858}
	Size4K      = Size{4096, 2160}
	Size4KDCI   = Size4K
	Size4KFlat  = Size{3996, 2160}
	Size4KScope = Size{4096, 1716}
	SizeNHD     = Size{640, 360}
	SizeHQVGA   = Size{240, 160}
	SizeWQVGA   = Size{400, 240}
	SizeFWQVGA  = Size{432, 240}
	SizeHVGA    = Size{480, 320}
	SizeQHD     = Size{960, 540}
	SizeUHD2160 = Size{3840, 2160}
	SizeUHD4320 = Size{7680, 4320}
)

var (
	FrameRateNTSC     = float64(30_000) / 1001
	FrameRatePAL      = 25
	FrameRateQNTSC    = FrameRateNTSC
	FrameRateQPAL     = FrameRatePAL
	FrameRateSNTSC    = FrameRateNTSC
	FrameRateSPAL     = FrameRatePAL
	FrameRateFilm     = 24
	FrameRateNTSCFilm = float64(24_000) / 1001
)

type Preset string

const (
	UltraFast Preset = "ultrafast"
	SuperFast Preset = "superfast"
	VeryFast  Preset = "veryfast"
	Faster    Preset = "faster"
	Fast      Preset = "fast"
	Medium    Preset = "medium"
	Slow      Preset = "slow"
	Slower    Preset = "slower"
	VerySlow  Preset = "veryslow"
)

var (
	ColorRandom = "random"

	ColorAliceBlue            = color.RGBA{0xf0, 0xf8, 0xff, 0xff}
	ColorAntiqueWhite         = color.RGBA{0xfa, 0xeb, 0xd7, 0xff}
	ColorAqua                 = color.RGBA{0x00, 0xff, 0xff, 0xff}
	ColorAquamarine           = color.RGBA{0x7f, 0xff, 0xd4, 0xff}
	ColorAzure                = color.RGBA{0xf0, 0xff, 0xff, 0xff}
	ColorBeige                = color.RGBA{0xf5, 0xf5, 0xdc, 0xff}
	ColorBisque               = color.RGBA{0xff, 0xe4, 0xc4, 0xff}
	ColorBlack                = color.RGBA{0x00, 0x00, 0x00, 0xff}
	ColorBlanchedAlmond       = color.RGBA{0xff, 0xeb, 0xcd, 0xff}
	ColorBlue                 = color.RGBA{0x00, 0x00, 0xff, 0xff}
	ColorBlueViolet           = color.RGBA{0x8a, 0x2b, 0xe2, 0xff}
	ColorBrown                = color.RGBA{0xa5, 0x2a, 0x2a, 0xff}
	ColorBurlyWood            = color.RGBA{0xde, 0xb8, 0x87, 0xff}
	ColorCadetBlue            = color.RGBA{0x5f, 0x9e, 0xa0, 0xff}
	ColorChartreuse           = color.RGBA{0x7f, 0xff, 0x00, 0xff}
	ColorChocolate            = color.RGBA{0xd2, 0x69, 0x1e, 0xff}
	ColorCoral                = color.RGBA{0xff, 0x7f, 0x50, 0xff}
	ColorCornflowerBlue       = color.RGBA{0x64, 0x95, 0xed, 0xff}
	ColorCornsilk             = color.RGBA{0xff, 0xf8, 0xdc, 0xff}
	ColorCrimson              = color.RGBA{0xdc, 0x14, 0x3c, 0xff}
	ColorCyan                 = color.RGBA{0x00, 0xff, 0xff, 0xff}
	ColorDarkBlue             = color.RGBA{0x00, 0x00, 0x8b, 0xff}
	ColorDarkCyan             = color.RGBA{0x00, 0x8b, 0x8b, 0xff}
	ColorDarkGoldenRod        = color.RGBA{0xb8, 0x86, 0x0b, 0xff}
	ColorDarkGray             = color.RGBA{0xa9, 0xa9, 0xa9, 0xff}
	ColorDarkGreen            = color.RGBA{0x00, 0x64, 0x00, 0xff}
	ColorDarkKhaki            = color.RGBA{0xbd, 0xb7, 0x6b, 0xff}
	ColorDarkMagenta          = color.RGBA{0x8b, 0x00, 0x8b, 0xff}
	ColorDarkOliveGreen       = color.RGBA{0x55, 0x6b, 0x2f, 0xff}
	ColorDarkorange           = color.RGBA{0xff, 0x8c, 0x00, 0xff}
	ColorDarkOrchid           = color.RGBA{0x99, 0x32, 0xcc, 0xff}
	ColorDarkRed              = color.RGBA{0x8b, 0x00, 0x00, 0xff}
	ColorDarkSalmon           = color.RGBA{0xe9, 0x96, 0x7a, 0xff}
	ColorDarkSeaGreen         = color.RGBA{0x8f, 0xbc, 0x8f, 0xff}
	ColorDarkSlateBlue        = color.RGBA{0x48, 0x3d, 0x8b, 0xff}
	ColorDarkSlateGray        = color.RGBA{0x2f, 0x4f, 0x4f, 0xff}
	ColorDarkTurquoise        = color.RGBA{0x00, 0xce, 0xd1, 0xff}
	ColorDarkViolet           = color.RGBA{0x94, 0x00, 0xd3, 0xff}
	ColorDeepPink             = color.RGBA{0xff, 0x14, 0x93, 0xff}
	ColorDeepSkyBlue          = color.RGBA{0x00, 0xbf, 0xff, 0xff}
	ColorDimGray              = color.RGBA{0x69, 0x69, 0x69, 0xff}
	ColorDodgerBlue           = color.RGBA{0x1e, 0x90, 0xff, 0xff}
	ColorFireBrick            = color.RGBA{0xb2, 0x22, 0x22, 0xff}
	ColorFloralWhite          = color.RGBA{0xff, 0xfa, 0xf0, 0xff}
	ColorForestGreen          = color.RGBA{0x22, 0x8b, 0x22, 0xff}
	ColorFuchsia              = color.RGBA{0xff, 0x00, 0xff, 0xff}
	ColorGainsboro            = color.RGBA{0xdc, 0xdc, 0xdc, 0xff}
	ColorGhostWhite           = color.RGBA{0xf8, 0xf8, 0xff, 0xff}
	ColorGold                 = color.RGBA{0xff, 0xd7, 0x00, 0xff}
	ColorGoldenRod            = color.RGBA{0xda, 0xa5, 0x20, 0xff}
	ColorGray                 = color.RGBA{0x80, 0x80, 0x80, 0xff}
	ColorGreen                = color.RGBA{0x00, 0x80, 0x00, 0xff}
	ColorGreenYellow          = color.RGBA{0xad, 0xff, 0x2f, 0xff}
	ColorHoneyDew             = color.RGBA{0xf0, 0xff, 0xf0, 0xff}
	ColorHotPink              = color.RGBA{0xff, 0x69, 0xb4, 0xff}
	ColorIndianRed            = color.RGBA{0xcd, 0x5c, 0x5c, 0xff}
	ColorIndigo               = color.RGBA{0x4b, 0x00, 0x82, 0xff}
	ColorIvory                = color.RGBA{0xff, 0xff, 0xf0, 0xff}
	ColorKhaki                = color.RGBA{0xf0, 0xe6, 0x8c, 0xff}
	ColorLavender             = color.RGBA{0xe6, 0xe6, 0xfa, 0xff}
	ColorLavenderBlush        = color.RGBA{0xff, 0xf0, 0xf5, 0xff}
	ColorLawnGreen            = color.RGBA{0x7c, 0xfc, 0x00, 0xff}
	ColorLemonChiffon         = color.RGBA{0xff, 0xfa, 0xcd, 0xff}
	ColorLightBlue            = color.RGBA{0xad, 0xd8, 0xe6, 0xff}
	ColorLightCoral           = color.RGBA{0xf0, 0x80, 0x80, 0xff}
	ColorLightCyan            = color.RGBA{0xe0, 0xff, 0xff, 0xff}
	ColorLightGoldenRodYellow = color.RGBA{0xfa, 0xfa, 0xd2, 0xff}
	ColorLightGreen           = color.RGBA{0x90, 0xee, 0x90, 0xff}
	ColorLightGrey            = color.RGBA{0xd3, 0xd3, 0xd3, 0xff}
	ColorLightPink            = color.RGBA{0xff, 0xb6, 0xc1, 0xff}
	ColorLightSalmon          = color.RGBA{0xff, 0xa0, 0x7a, 0xff}
	ColorLightSeaGreen        = color.RGBA{0x20, 0xb2, 0xaa, 0xff}
	ColorLightSkyBlue         = color.RGBA{0x87, 0xce, 0xfa, 0xff}
	ColorLightSlateGray       = color.RGBA{0x77, 0x88, 0x99, 0xff}
	ColorLightSteelBlue       = color.RGBA{0xb0, 0xc4, 0xde, 0xff}
	ColorLightYellow          = color.RGBA{0xff, 0xff, 0xe0, 0xff}
	ColorLime                 = color.RGBA{0x00, 0xff, 0x00, 0xff}
	ColorLimeGreen            = color.RGBA{0x32, 0xcd, 0x32, 0xff}
	ColorLinen                = color.RGBA{0xfa, 0xf0, 0xe6, 0xff}
	ColorMagenta              = color.RGBA{0xff, 0x00, 0xff, 0xff}
	ColorMaroon               = color.RGBA{0x80, 0x00, 0x00, 0xff}
	ColorMediumAquaMarine     = color.RGBA{0x66, 0xcd, 0xaa, 0xff}
	ColorMediumBlue           = color.RGBA{0x00, 0x00, 0xcd, 0xff}
	ColorMediumOrchid         = color.RGBA{0xba, 0x55, 0xd3, 0xff}
	ColorMediumPurple         = color.RGBA{0x93, 0x70, 0xd8, 0xff}
	ColorMediumSeaGreen       = color.RGBA{0x3c, 0xb3, 0x71, 0xff}
	ColorMediumSlateBlue      = color.RGBA{0x7b, 0x68, 0xee, 0xff}
	ColorMediumSpringGreen    = color.RGBA{0x00, 0xfa, 0x9a, 0xff}
	ColorMediumTurquoise      = color.RGBA{0x48, 0xd1, 0xcc, 0xff}
	ColorMediumVioletRed      = color.RGBA{0xc7, 0x15, 0x85, 0xff}
	ColorMidnightBlue         = color.RGBA{0x19, 0x19, 0x70, 0xff}
	ColorMintCream            = color.RGBA{0xf5, 0xff, 0xfa, 0xff}
	ColorMistyRose            = color.RGBA{0xff, 0xe4, 0xe1, 0xff}
	ColorMoccasin             = color.RGBA{0xff, 0xe4, 0xb5, 0xff}
	ColorNavajoWhite          = color.RGBA{0xff, 0xde, 0xad, 0xff}
	ColorNavy                 = color.RGBA{0x00, 0x00, 0x80, 0xff}
	ColorOldLace              = color.RGBA{0xfd, 0xf5, 0xe6, 0xff}
	ColorOlive                = color.RGBA{0x80, 0x80, 0x00, 0xff}
	ColorOliveDrab            = color.RGBA{0x6b, 0x8e, 0x23, 0xff}
	ColorOrange               = color.RGBA{0xff, 0xa5, 0x00, 0xff}
	ColorOrangeRed            = color.RGBA{0xff, 0x45, 0x00, 0xff}
	ColorOrchid               = color.RGBA{0xda, 0x70, 0xd6, 0xff}
	ColorPaleGoldenRod        = color.RGBA{0xee, 0xe8, 0xaa, 0xff}
	ColorPaleGreen            = color.RGBA{0x98, 0xfb, 0x98, 0xff}
	ColorPaleTurquoise        = color.RGBA{0xaf, 0xee, 0xee, 0xff}
	ColorPaleVioletRed        = color.RGBA{0xd8, 0x70, 0x93, 0xff}
	ColorPapayaWhip           = color.RGBA{0xff, 0xef, 0xd5, 0xff}
	ColorPeachPuff            = color.RGBA{0xff, 0xda, 0xb9, 0xff}
	ColorPeru                 = color.RGBA{0xcd, 0x85, 0x3f, 0xff}
	ColorPink                 = color.RGBA{0xff, 0xc0, 0xcb, 0xff}
	ColorPlum                 = color.RGBA{0xdd, 0xa0, 0xdd, 0xff}
	ColorPowderBlue           = color.RGBA{0xb0, 0xe0, 0xe6, 0xff}
	ColorPurple               = color.RGBA{0x80, 0x00, 0x80, 0xff}
	ColorRed                  = color.RGBA{0xff, 0x00, 0x00, 0xff}
	ColorRosyBrown            = color.RGBA{0xbc, 0x8f, 0x8f, 0xff}
	ColorRoyalBlue            = color.RGBA{0x41, 0x69, 0xe1, 0xff}
	ColorSaddleBrown          = color.RGBA{0x8b, 0x45, 0x13, 0xff}
	ColorSalmon               = color.RGBA{0xfa, 0x80, 0x72, 0xff}
	ColorSandyBrown           = color.RGBA{0xf4, 0xa4, 0x60, 0xff}
	ColorSeaGreen             = color.RGBA{0x2e, 0x8b, 0x57, 0xff}
	ColorSeaShell             = color.RGBA{0xff, 0xf5, 0xee, 0xff}
	ColorSienna               = color.RGBA{0xa0, 0x52, 0x2d, 0xff}
	ColorSilver               = color.RGBA{0xc0, 0xc0, 0xc0, 0xff}
	ColorSkyBlue              = color.RGBA{0x87, 0xce, 0xeb, 0xff}
	ColorSlateBlue            = color.RGBA{0x6a, 0x5a, 0xcd, 0xff}
	ColorSlateGray            = color.RGBA{0x70, 0x80, 0x90, 0xff}
	ColorSnow                 = color.RGBA{0xff, 0xfa, 0xfa, 0xff}
	ColorSpringGreen          = color.RGBA{0x00, 0xff, 0x7f, 0xff}
	ColorSteelBlue            = color.RGBA{0x46, 0x82, 0xb4, 0xff}
	ColorTan                  = color.RGBA{0xd2, 0xb4, 0x8c, 0xff}
	ColorTeal                 = color.RGBA{0x00, 0x80, 0x80, 0xff}
	ColorThistle              = color.RGBA{0xd8, 0xbf, 0xd8, 0xff}
	ColorTomato               = color.RGBA{0xff, 0x63, 0x47, 0xff}
	ColorTurquoise            = color.RGBA{0x40, 0xe0, 0xd0, 0xff}
	ColorViolet               = color.RGBA{0xee, 0x82, 0xee, 0xff}
	ColorWheat                = color.RGBA{0xf5, 0xde, 0xb3, 0xff}
	ColorWhite                = color.RGBA{0xff, 0xff, 0xff, 0xff}
	ColorWhiteSmoke           = color.RGBA{0xf5, 0xf5, 0xf5, 0xff}
	ColorYellow               = color.RGBA{0xff, 0xff, 0x00, 0xff}
	ColorYellowGreen          = color.RGBA{0x9a, 0xcd, 0x32, 0xff}
)

type Channel string

const (
	ChannelFL   Channel = "FL"   // front left
	ChannelFR   Channel = "FR"   // front right
	ChannelFC   Channel = "FC"   // front center
	ChannelLFE  Channel = "LFE"  // low frequency
	ChannelBL   Channel = "BL"   // back left
	ChannelBR   Channel = "BR"   // back right
	ChannelFLC  Channel = "FLC"  // front left-of-center
	ChannelFRC  Channel = "FRC"  // front right-of-center
	ChannelBC   Channel = "BC"   // back center
	ChannelSL   Channel = "SL"   // side left
	ChannelSR   Channel = "SR"   // side right
	ChannelTC   Channel = "TC"   // top center
	ChannelTFL  Channel = "TFL"  // top front left
	ChannelTFC  Channel = "TFC"  // top front center
	ChannelTFR  Channel = "TFR"  // top front right
	ChannelTBL  Channel = "TBL"  // top back left
	ChannelTBC  Channel = "TBC"  // top back center
	ChannelTBR  Channel = "TBR"  // top back right
	ChannelDL   Channel = "DL"   // downmix left
	ChannelDR   Channel = "DR"   // downmix right
	ChannelWL   Channel = "WL"   // wide left
	ChannelWR   Channel = "WR"   // wide right
	ChannelSDL  Channel = "SDL"  // surround direct left
	ChannelSDR  Channel = "SDR"  // surround direct right
	ChannelLFE2 Channel = "LFE2" // low frequency 2
	ChannelTSL  Channel = "TSL"  // top side left
	ChannelTSR  Channel = "TSR"  // top side right
	ChannelBFC  Channel = "BFC"  // bottom front center
	ChannelBFL  Channel = "BFL"  // bottom front left
	ChannelBFR  Channel = "BFR"  // bottom front right
)

func ChannelLayout(channels ...Channel) Channel {
	str := make([]string, len(channels))
	for i, c := range channels {
		str[i] = string(c)
	}
	return Channel(strings.Join(str, "+"))
}

var (
	ChannelLayoutMono          = ChannelLayout(ChannelFC)                                                                                                                                                                                                                             // mono
	ChannelLayoutStereo        = ChannelLayout(ChannelFL, ChannelFR)                                                                                                                                                                                                                  // stereo
	ChannelLayout2P1           = ChannelLayout(ChannelLayoutStereo, ChannelLFE)                                                                                                                                                                                                       // 2.1
	ChannelLayout3P0           = ChannelLayout(ChannelLayoutStereo, ChannelFC)                                                                                                                                                                                                        // 3.0
	ChannelLayout3P0Back       = ChannelLayout(ChannelLayoutStereo, ChannelBC)                                                                                                                                                                                                        // 3.0(back)
	ChannelLayout4P0           = ChannelLayout(ChannelLayout3P0, ChannelBC)                                                                                                                                                                                                           // 4.0
	ChannelLayoutQuad          = ChannelLayout(ChannelLayoutStereo, ChannelBL, ChannelBR)                                                                                                                                                                                             // quad
	ChannelLayoutQuadSide      = ChannelLayout(ChannelLayoutStereo, ChannelSL, ChannelSR)                                                                                                                                                                                             // quad(side)
	ChannelLayout3P1           = ChannelLayout(ChannelLayout3P0, ChannelLFE)                                                                                                                                                                                                          // 3.1
	ChannelLayout5P0           = ChannelLayout(ChannelLayout3P0, ChannelBL, ChannelBR)                                                                                                                                                                                                // 5.0
	ChannelLayout5P0Side       = ChannelLayout(ChannelLayout3P0, ChannelSL, ChannelSR)                                                                                                                                                                                                // 5.0(side)
	ChannelLayout4P1           = ChannelLayout(ChannelLayout4P0, ChannelLFE)                                                                                                                                                                                                          // 4.1
	ChannelLayout5P1           = ChannelLayout(ChannelLayout5P0, ChannelLFE)                                                                                                                                                                                                          // 5.1
	ChannelLayout5P1Side       = ChannelLayout(ChannelLayout5P0Side, ChannelLFE)                                                                                                                                                                                                      // 5.1(side)
	ChannelLayout6P0           = ChannelLayout(ChannelLayout5P0Side, ChannelBC)                                                                                                                                                                                                       // 6.0
	ChannelLayout6P0Front      = ChannelLayout(ChannelLayoutQuadSide, ChannelFLC, ChannelFRC)                                                                                                                                                                                         // 6.0(front)
	ChannelLayoutHexagonal     = ChannelLayout(ChannelLayout5P0, ChannelBC)                                                                                                                                                                                                           // hexagonal
	ChannelLayout6P1           = ChannelLayout(ChannelLayout5P1Side, ChannelBC)                                                                                                                                                                                                       // 6.1
	ChannelLayout6P1Back       = ChannelLayout(ChannelLayout5P1, ChannelBC)                                                                                                                                                                                                           // 6.1(back)
	ChannelLayout6P1Front      = ChannelLayout(ChannelLayout6P0Front, ChannelLFE)                                                                                                                                                                                                     // 6.1(front)
	ChannelLayout7P0           = ChannelLayout(ChannelLayout5P0Side, ChannelBL, ChannelBR)                                                                                                                                                                                            // 7.0
	ChannelLayout7P0Front      = ChannelLayout(ChannelLayout5P0Side, ChannelFLC, ChannelFRC)                                                                                                                                                                                          // 7.0(front)
	ChannelLayout7P1           = ChannelLayout(ChannelLayout5P1Side, ChannelBL, ChannelBR)                                                                                                                                                                                            // 7.1
	ChannelLayout7P1Wide       = ChannelLayout(ChannelLayout5P1, ChannelFLC, ChannelFRC)                                                                                                                                                                                              // 7.1(wide)
	ChannelLayout7P1WideSide   = ChannelLayout(ChannelLayout5P1Side, ChannelFLC, ChannelFRC)                                                                                                                                                                                          // 7.1(wide-side)
	ChannelLayoutOctagonal     = ChannelLayout(ChannelLayout5P0Side, ChannelBL, ChannelBC, ChannelBR)                                                                                                                                                                                 // octagonal
	ChannelLayoutHexadecagonal = ChannelLayout(ChannelLayoutOctagonal, ChannelWL, ChannelWR, ChannelTBL, ChannelTBR, ChannelTBC, ChannelTFC, ChannelTFL, ChannelTFR)                                                                                                                  // hexadecagonal
	ChannelLayoutDownmix       = ChannelLayout(ChannelDL, ChannelDR)                                                                                                                                                                                                                  // downmix
	ChannelLayout22P2          = ChannelLayout(ChannelLayout5P1, ChannelFLC, ChannelFRC, ChannelBC, ChannelLFE2, ChannelSL, ChannelSR, ChannelTFL, ChannelTFR, ChannelTFC, ChannelTC, ChannelTBL, ChannelTBR, ChannelTSL, ChannelTSR, ChannelTBC, ChannelBFC, ChannelBFL, ChannelBFR) // 22.2
)
