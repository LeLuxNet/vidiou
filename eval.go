package vidiou

import "math"

const (
	VEval_Frame  = "N"
	VEval_Time   = "T"
	VEval_X      = "X"
	VEval_Y      = "Y"
	VEval_Width  = "W"
	VEval_Height = "H"
)

func VEval_Luminance(x, y Expression) Expression {
	return Func("lum", x, y)
}

func VEval_ChrominanceBlue(x, y Expression) Expression {
	return Func("cb", x, y)
}

func VEval_ChrominanceRed(x, y Expression) Expression {
	return Func("cr", x, y)
}

func VEval_Alpha(x, y Expression) Expression {
	return Func("a", x, y)
}

func VEval_Red(x, y Expression) Expression {
	return Func("r", x, y)
}

func VEval_Green(x, y Expression) Expression {
	return Func("g", x, y)
}

func VEval_Blue(x, y Expression) Expression {
	return Func("b", x, y)
}

type VEval struct {
	Input           Node
	Luminance       Expression
	ChrominanceBlue Expression
	ChrominanceRed  Expression
	Alpha           Expression
	Red             Expression
	Green           Expression
	Blue            Expression
}

func (n VEval) parents() []Node {
	return []Node{n.Input}
}

func (n VEval) streamType() StreamType {
	return Video
}

func (f VEval) cfArgs() (string, FilterOptions) {
	return "geq", FilterOptions{
		"lum": f.Luminance,
		"cb":  f.ChrominanceBlue,
		"cr":  f.ChrominanceRed,
		"a":   f.Alpha,
		"r":   f.Red,
		"g":   f.Green,
		"b":   f.Blue,
	}
}

const (
	AEval_Frame      = "n"
	AEval_Time       = "t"
	AEval_SampleRate = "r"
	AEval_Channel    = "ch"

	AEval_ChannelSame = "same"
)

func AEval_Value(ch Expression) Expression {
	if ch == nil {
		ch = AEval_Channel
	}
	return Func("val", ch)
}

func AEval_Sine(freq Expression) Expression {
	return Sin(Mul(2*math.Pi, freq, AEval_Time))
}

func AEval_Square(freq Expression) Expression {
	return If(Round(Mod(Mul(AEval_Time, freq), 1)), -1, 1)
}

func AEval_Triangle(freq Expression) Expression {
	tf := Mul(AEval_Time, freq)
	return Sub(Mul(2, Mul(2, Abs(Sub(tf, Round(tf))))), 1)
}

func AEval_Sawtooth(freq Expression) Expression {
	tf := Mul(AEval_Time, freq)
	return Mul(2, Sub(tf, Round(tf)))
}

type AEval struct {
	Input         Node
	Expressions   []Expression
	ChannelLayout Channel
	Duration      Expression
	SampleRate    Expression
}

func (n *AEval) streamType() StreamType {
	return Audio
}

func (n *AEval) parents() []Node {
	if n.Input == nil {
		return nil
	}
	return []Node{n.Input}
}

func (f *AEval) cfArgs() (string, FilterOptions) {
	name := "aevalsrc"
	if f.Input != nil {
		name = "aeval"
	}
	return name, FilterOptions{
		"exprs": arithmetic(f.Expressions, "|"),
		"c":     f.ChannelLayout,
		"d":     f.Duration,
		"s":     f.SampleRate,
	}
}
