package vidiou

type Negate struct {
	Input       Node
	NegateAlpha Expression
}

func (n Negate) parents() []Node {
	return []Node{n.Input}
}

func (n Negate) streamType() StreamType {
	return Video
}

func (n Negate) cfArgs() (string, FilterOptions) {
	return "negate", FilterOptions{
		"negate_alpha": n.NegateAlpha,
	}
}
